/*
 * Keyu Liu
 * 110801487
 * Homework 3
 * cse214 recitation 01
 * Recitation TA: Charles Chen
 * Grading TA:
 * @author LKY
 */
 
import java.util.Stack;

public class JavascriptFormatter {

    /**
     * set a JSStack named stack set indent level number
     */
    private JSStack stack;
    private int indentLevel;

    /**
     * get the data and format it
     *
     * @param input
     * @return
     */
    public String format(String input) {
        String[] c = input.split("\t|\n| ");
        String toReturn = "";
        boolean cinfor = false, cb4for = true;

        for (int i = 0; i < c.length; i++) {
            if (c[i].contains("for(")) {
                cinfor = true;
            }
            
            /**
             * create a for loop to read all the string
             */
            for (int j = 0; j < c[i].length(); j++) {
                String x = Character.toString(c[i].charAt(j));
                
                /**
                 * test the missing of extra cases
                 * fix the cases
                 */
                switch (x) {
                    case "f":
                        if (cinfor && c[i].substring(j, j + 4).contains("for(")) {
                            stack.push(BlockType.FOR);
                            cb4for = false;
                        }
                        break;

                    case "{":
                        stack.push(BlockType.BRACE);
                        indentLevel++;
                        c[i] = c[i].substring(0, j + 1) + "\n" + indent(indentLevel) + c[i].substring(j + 1, c[i].length());

                        break;

                    case "(":
                        if (cb4for) {
                            stack.push(BlockType.PAREN);
                        }
                        break;

                    case "}":
                        indentLevel--;
                        if (stack.peek() == BlockType.BRACE) {
                            c[i] = c[i].substring(0, j + 1) + "\n" + indent(indentLevel) + c[i].substring(j + 1, c[i].length());
                            stack.pop();
                        } else if (stack.peek() == BlockType.PAREN || stack.peek() == BlockType.FOR) {
                            c[i] = c[i].substring(0, j + 1) + ")" + c[i].substring(j + 1, c[i].length());;
                        }

                        break;

                    case ")":
                        if (stack.peek() == BlockType.PAREN) {
                            stack.pop();
                        } else if (stack.peek() == BlockType.FOR) {
                            cinfor = false;
                            cb4for = true;
                            stack.pop();
                        } else {
                            toReturn += ")";
                        }
                        break;

                    case ";":
                        if(!stack.isEmpty() && stack.peek() == BlockType.PAREN) {
                            c[i] = c[i].substring(0, j ) + ")" + c[i].substring(j, c[i].length());
                            stack.pop();
                            break;
                        }
                        if (cinfor && !cb4for) {
                            break;
                        }

                        c[i] = c[i].substring(0, j + 1) + "\n" + indent(indentLevel) + c[i].substring(j + 1, c[i].length());
                        break;
                }
            }
            toReturn += c[i] + " ";
        }
        
        /**
         * add the laking items
         */
        if (!stack.isEmpty()) {
            switch (stack.peek()) {
                case BRACE:
                    toReturn = toReturn + "\n}";
                    break;
                case PAREN:
                    toReturn += ")";
                    break;
                case FOR:
                    toReturn += ")";
                    break;
            }
        }
        return toReturn;
    }

    /**
    * provide the method to find the indent level
    * @param indentlevel
    * @return 
    */
    private String indent(int indentlevel) {
        String result = "";
        for (int i = 0; i < indentlevel; i++) {
            result += "\t";
        }
        return result;
    }

    /**
    * build the constructor for this class
    */
    public JavascriptFormatter() {
        stack = new JSStack();
        indentLevel = 0;
    }

}