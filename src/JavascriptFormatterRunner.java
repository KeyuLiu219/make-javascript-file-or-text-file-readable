/*
 * Keyu Liu
 * 110801487
 * Homework 3
 * cse214 recitation 01
 * Recitation TA: Charles Chen
 * Grading TA:
 * @author LKY
 */

import java.util.*;
import java.io.*;

/**
 * read the file
 * @author Keyu
 */
public class JavascriptFormatterRunner {
	public static void main(String[] args){
		String n = "";
		String filePath = "";
		
		System.out.println("Welcome to the Javascript Formatter.");
                /**
                 * input the file name
                 */
		Scanner name = new Scanner(System.in);
		System.out.print("Please Enter a filename: ");
		n = "src/"+name.nextLine();
                String inPut = " ";
                 try{
                         File file = new File(n);
                         Scanner inputFile = new Scanner(file);
                         System.out.println(" ");
                         System.out.println("------- Properly formatted program -------");
                         while(inputFile.hasNext()){
                            inPut += inputFile.nextLine();}
                         
                         JavascriptFormatter fileRun = new JavascriptFormatter();
                         String k = fileRun.format(inPut);
                         System.out.println(k);
                         inputFile.close();
                         System.out.println("--Thank you for making your code readable!--");
                 }
                 /**
                  * find the exception
                  */
                 catch(Exception e){
                     System.out.println("file cannot be found.");
                 }

	}
}


