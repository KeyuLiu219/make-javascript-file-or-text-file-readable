/*
 * Keyu Liu
 * 110801487
 * Homework 3
 * cse214 recitation 01
 * Recitation TA: Charles Chen
 * Grading TA:
 * @author LKY
 */

/**
 * 
 * create the enumeration
 */
public enum BlockType {
    BRACE, PAREN, FOR
}
