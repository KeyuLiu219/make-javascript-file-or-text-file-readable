/*
 * Keyu Liu
 * 110801487
 * Homework 3
 * cse214 recitation 01
 * Recitation TA: Charles Chen
 * Grading TA:
 * @author LKY
 */

/**
 * set the stack from the java stack;
 */
import java.util.Stack;
public class JSStack implements Cloneable{
	private Stack inputs;
	
        /**
         * initialize the stack
         */
	public JSStack(){
		inputs = new Stack();
	}
	
        /**
         * provide push method
         * @param b 
         */
	public void push(BlockType b){
		inputs.push(b);
	}
	
        /**
         * provide pop method
         * @return 
         */
	public BlockType pop(){
		return (BlockType)inputs.pop();
		
	}
	
        /**
         * provide peek method
         * @return 
         */
	public BlockType peek(){
		return (BlockType)inputs.peek();
		
	}
	
        /**
         * provide isEmpoty method to test if the stack is empty
         * @return 
         */
	public boolean isEmpty(){
		return (boolean)inputs.isEmpty() ;
	}
}

